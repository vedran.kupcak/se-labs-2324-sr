import csv

# Initialize an empty list to store the employee data
employees = []

# Open the CSV file for reading
with open('ex2-text.csv', newline='') as csvfile:
    # Create a CSV reader object
    csv_reader = csv.DictReader(csvfile)
    
    # Iterate through the rows in the CSV file
    for row in csv_reader:
        # Create a dictionary for each employee and add it to the list
        employee_data = {
            'employee': row['employee'],
            'title': row['title'],
            'age': int(row['age']),  # Convert age to an integer
            'office': row['office']
        }
        employees.append(employee_data)

# Print the list of employees
print(employees)
