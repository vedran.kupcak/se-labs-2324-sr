import json

class Employee:

    def __init__(self, name, title, age, office):
        self.name = name
        self.title = title
        self.age = age
        self.office = office

    def __str__(self):
        return f"{self.name} ({self.age}), {self.title} @ {self.office}"

# Load employees from ex4-employees.json file
with open("ex4-employees.json", "r", encoding="utf-8") as f:
    employees_data = json.load(f)

# Create a list employees_list with Employee objects
employees_list = []

for employee_data in employees_data:
    employee = Employee(employee_data['employee'], employee_data['title'], employee_data['age'], employee_data['office'])
    employees_list.append(employee)

# Print the employee information
for e in employees_list:
    print(e)
