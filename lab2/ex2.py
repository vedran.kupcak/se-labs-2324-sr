import csv

# Initialize empty lists to store data
employees = []
locations = []

# Read data from the CSV file
with open('ex2-text.csv', 'r') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for row in csv_reader:
        if len(row) == 2: # Ensure there are exactly two values in each row
            employees.append(row[0])
            locations.append(row[1])

# Write data to 'ex2-employees.txt'
with open('ex2-employees.txt', 'w') as employees_file:
    for employee, job_title in zip(employees, locations):
        employees_file.write(f"{employee},{job_title}\n")

# Write data to 'ex2-locations.txt'
with open('ex2-locations.txt', 'w') as locations_file:
    for employee, office in zip(employees, locations):
        locations_file.write(f"{employee},{office}\n")

print("Data has been successfully written to ex2-employees.txt and ex2-locations.txt.")