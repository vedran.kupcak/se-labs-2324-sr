# Ask the user for their name
name = input("Please enter your name: ")

# Using formatted string literals (f-strings)
greeting_fstring = f"Hi {name}!"
print("Using formatted string literals:")
print(greeting_fstring)

# Using the String `format()` method
greeting_format = "Hi {}!".format(name)
print("Using the String `format()` method:")
print(greeting_format)

# Using the old string formatting method
greeting_old_format = "Hi %s!" % name
print("Using the old string formatting method:")
print(greeting_old_format)
