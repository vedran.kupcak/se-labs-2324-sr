def has_adjacent_twos(nums):
    for i in range(len(nums) - 1):
        if nums[i] == 2 and nums[i + 1] == 2:
            return True
    return False


input_str = input("Enter a comma-separated list of integers: ")


int_list = [int(num) for num in input_str.split(',')]


result = has_adjacent_twos(int_list)
print("Contains adjacent twos:", result)
