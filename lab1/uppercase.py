from ex1 import count_evens
from ex2 import centered_average
from ex3 import has_adjacent_twos
from ex4 import char_types_count

def uppercase_count(string):


    count = 0;
    allowed_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    for char in string:
        if char in allowed_chars:
            count += 1

    return count

def main():
    strings = [
            "pero", 
            "Pero",
            "PEROPERO",
            "WySiWyG"
    ]   

    for string in strings:
        print(f"String '{string}' has {uppercase_count(string)} uppercase letters")

print(centered_average([1, 2, 3, 4, 100]))  
print(centered_average([1, 1, 5, 5, 10, 8, 7]))  
print(centered_average([-10, -4, -2, -4, -2, 0])) 

result = char_types_count("asdf98CXX21grrr")
print(result)

if __name__ == "__main__":
    main()
