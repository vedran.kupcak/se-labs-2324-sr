def count_evens(nums):

    even_count = 0


    for num in nums:

        if num % 2 == 0:
            even_count += 1


    return even_count


input_str = input("Enter a comma-separated list of integers: ")

int_list = [int(num) for num in input_str.split(',')]


result = count_evens(int_list)
print("Nuber of even integers:", result)
